package gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;

import java.awt.*;
import java.io.*;

import javafx.scene.control.TextArea;


import dissimlab.monitors.Diagram;
import dissimlab.monitors.Diagram.DiagramType;
import dissimlab.monitors.Statistics;
import dissimlab.random.SimGenerator;
import dissimlab.simcore.SimControlEvent;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimManager;
import dissimlab.simcore.SimParameters;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import symulacja.*;


public class Controller {

    private App symulacja;

    private @FXML TextField liczbaOkienekField;
    private  @FXML TextField miField;
    private @FXML TextField lambdaField;
    private @FXML TextField liczbaPriorytetowField;
    private @FXML TextField limitKlientowField;
    private @FXML TextField niezdatnoscField;
    private @FXML TextField zdatnoscField;
    private @FXML TextField szansaPowrotuField;
    private @FXML TextField zniecierpliwienieField;
    private @FXML TextField czasField;
    private @FXML TextArea wynikSym;

    private int liczbaOkienek, liczbaPriorytetow, limitKlientow, szansaPowrotu, czas;

    private double mi, lambda, niezdatnosc, zdatnosc, zniecierpliwienie;

    private Diagram d1;
    private Diagram d2;
    private Diagram d3;
    private Diagram d4;
    private Diagram d5;



    public void pressReadFromFile(ActionEvent event){

        JSONParser jsonParser = new JSONParser();

        try(FileReader reader = new FileReader("D:\\Studia\\Semestr 5\\Podstawy symulacji\\Laboratoria\\projekt\\Git\\src\\gui\\dane.json"))
        {
            Object obj = jsonParser.parse(reader);

            JSONObject jsonObj = (org.json.simple.JSONObject) obj;

            liczbaOkienek = (int) (long) jsonObj.get("liczbaOkienek");
            mi = (double) jsonObj.get("mi");
            lambda = (double)jsonObj.get("lambda");
            liczbaPriorytetow = (int)(long)jsonObj.get("liczbaPriorytetow");
            limitKlientow= (int)(long)jsonObj.get("limitKlientow");
            niezdatnosc = (double)jsonObj.get("niezdatnosc");
            zdatnosc = (double)jsonObj.get("zdatnosc");
            szansaPowrotu = (int)(long)jsonObj.get("szansaPowrotu");
            zniecierpliwienie = (double)jsonObj.get("zniecierpliwienie");
            czas = (int)(long)jsonObj.get("czas");


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        /*System.out.println("liczbaOkienek = " + liczbaOkienek
        + ", mi = " + mi
        + ", lambda = " + lambda
        + ", liczbaPriorytetow = " + liczbaPriorytetow
        + ", limitKlientow = " + limitKlientow
        + ", niezdatnosc = " + niezdatnosc
        + ", zdatnosc = " + zdatnosc
        + ", szansaPowrotu = " + szansaPowrotu
        + ", zniecierpliwienie = " + zniecierpliwienie
        + ", czas = " + czas);*/


       liczbaOkienekField.setPromptText(Integer.toString(liczbaOkienek));
       miField.setPromptText(Double.toString(mi));
       lambdaField.setPromptText(Double.toString(lambda));
       liczbaPriorytetowField.setPromptText(Integer.toString(liczbaPriorytetow));
       limitKlientowField.setPromptText(Integer.toString(limitKlientow));
       niezdatnoscField.setPromptText(Double.toString(niezdatnosc));
       zdatnoscField.setPromptText(Double.toString(zdatnosc));
       szansaPowrotuField.setPromptText(Integer.toString(szansaPowrotu));
       zniecierpliwienieField.setPromptText(Double.toString(zniecierpliwienie));
       czasField.setPromptText(Integer.toString(czas));
    }

    public void pressStartButton(ActionEvent event){

        //pobranie danych z pól
        if(liczbaOkienekField.getText().isEmpty() && liczbaOkienekField.getPromptText().isEmpty()
            && miField.getText().isEmpty() && miField.getPromptText().isEmpty()
            && lambdaField.getText().isEmpty() && lambdaField.getPromptText().isEmpty()
            && liczbaPriorytetowField.getText().isEmpty() && liczbaPriorytetowField.getPromptText().isEmpty()
            && limitKlientowField.getText().isEmpty() && limitKlientowField.getPromptText().isEmpty()
            && niezdatnoscField.getText().isEmpty() && niezdatnoscField.getPromptText().isEmpty()
            && zdatnoscField.getText().isEmpty() && zdatnoscField.getPromptText().isEmpty()
            && szansaPowrotuField.getText().isEmpty() && szansaPowrotuField.getPromptText().isEmpty()
            && zniecierpliwienieField.getText().isEmpty() && zniecierpliwienieField.getPromptText().isEmpty()
            && czasField.getText().isEmpty() && czasField.getPromptText().isEmpty()){

            //wypisanie w TextArea informacji o konieczności wypełnienia pól lub pobrania danych z pliku
            //wynikSym.append("Podaj parametry symulacji.");
            wynikSym.appendText("Podaj parametry sumulacji.\n");
        }

        else{

            //pobranie danych z pól

            if(!liczbaOkienekField.getText().isEmpty())
                liczbaOkienek = Integer.parseInt(liczbaOkienekField.getText());

            if(!miField.getText().isEmpty())
                mi = Double.parseDouble(miField.getText());

            if(!lambdaField.getText().isEmpty())
                lambda = Double.parseDouble(lambdaField.getText());

            if(!liczbaPriorytetowField.getText().isEmpty())
                liczbaPriorytetow = Integer.parseInt(liczbaPriorytetowField.getText());


            if(!limitKlientowField.getText().isEmpty())
                limitKlientow = Integer.parseInt(limitKlientowField.getText());


            if(!niezdatnoscField.getText().isEmpty())
                niezdatnosc = Double.parseDouble(niezdatnoscField.getText());

            if(!zdatnoscField.getText().isEmpty())
                zdatnosc = Double.parseDouble(zdatnoscField.getText());

            if(!szansaPowrotuField.getText().isEmpty())
                szansaPowrotu = Integer.parseInt(szansaPowrotuField.getText());

            if(!zniecierpliwienieField.getText().isEmpty())
                zniecierpliwienie = Double.parseDouble(zniecierpliwienieField.getText());

            if(!czasField.getText().isEmpty())
                czas = Integer.parseInt(czasField.getText());


            symulacja = new App(this);
            symulacja.start();

        }


    }

    public void print(String text){

       // Platform.runLater(() -> {
            wynikSym.appendText(text + "\n");
       // });

    }

    public void pokazWykres1(ActionEvent event){
        d1.show();
    }

    public void pokazWykres2(ActionEvent event){
        d2.show();
    }

    public void pokazWykres3(ActionEvent event){
        d3.show();
    }

    public void pokazWykres4(ActionEvent event){
        d4.show();
    }

    public void pokazWykres5(ActionEvent event){
        d5.show();
    }


    public int getLiczbaOkienek() {
        return liczbaOkienek;
    }

    public int getLiczbaPriorytetow() {
        return liczbaPriorytetow;
    }

    public int getLimitKlientow() {
        return limitKlientow;
    }

    public int getSzansaPowrotu() {
        return szansaPowrotu;
    }

    public int getCzas() {
        return czas;
    }

    public double getMi() {
        return mi;
    }

    public double getLambda() {
        return lambda;
    }

    public double getNiezdatnosc() {
        return niezdatnosc;
    }

    public double getZdatnosc() {
        return zdatnosc;
    }

    public double getZniecierpliwienie() {
        return zniecierpliwienie;
    }

    public Diagram getD1() {
        return d1;
    }

    public Diagram getD2() {
        return d2;
    }

    public Diagram getD3() {
        return d3;
    }

    public Diagram getD4() {
        return d4;
    }

    public Diagram getD5() {
        return d5;
    }

    public void setD1(Diagram d1) {
        this.d1 = d1;
    }

    public void setD2(Diagram d2) {
        this.d2 = d2;
    }

    public void setD3(Diagram d3) {
        this.d3 = d3;
    }

    public void setD4(Diagram d4) {
        this.d4 = d4;
    }

    public void setD5(Diagram d5) {
        this.d5 = d5;
    }

}
