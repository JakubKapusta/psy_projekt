module Git {
    requires javafx.fxml;
    requires  javafx.controls;
    requires dissimlab;
    requires java.base;
    requires java.desktop;
    requires json;
    requires json.simple;

    opens gui;

}