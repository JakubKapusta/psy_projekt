package symulacja;

import java.awt.Color;

import dissimlab.monitors.Diagram;
import dissimlab.monitors.Diagram.DiagramType;
import dissimlab.monitors.MonitoredVar;
import dissimlab.monitors.Statistics;
import dissimlab.random.SimGenerator;
import dissimlab.simcore.SimControlEvent;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimManager;
import dissimlab.simcore.SimParameters;
import gui.Controller;

public class App {

	public static Controller controller;

	public App(Controller controller){
		this.controller = controller;
	}

    public void start(){
		//rozpocz�cie symulacji
		SimManager mgr = SimManager.getInstance();
		SimGenerator sg = new SimGenerator();


		Bank bank = new Bank(controller.getMi(), controller.getLiczbaPriorytetow(),controller.getLiczbaOkienek(), controller.getSzansaPowrotu(),controller.getLimitKlientow(), sg, controller.getZdatnosc(), controller.getNiezdatnosc(), controller.getZniecierpliwienie());


		Otoczenie otoczenie = new Otoczenie(bank, sg, controller.getLambda());
		try {
			new PrzybycieKlienta(otoczenie, 0);
			new WystapienieAwarii(bank, sg.exponential(bank.getNiezdatnosc()));
			new SimControlEvent(controller.getCzas(), SimParameters.SimControlStatus.STOPSIMULATION);
		} catch (SimControlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		try {
			mgr.startSimulation();
		} catch (SimControlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		//�redni czas obs�ugi
		controller.setD1(new Diagram(DiagramType.HISTOGRAM, "Czas obslugi"));
		controller.getD1().add(bank.getCzasObslugi(), Color.GREEN);


		//�rednia liczba klient�w w kolejce
		controller.setD2(new Diagram(DiagramType.HISTOGRAM, "Liczba klientow w kolejce"));
		controller.getD2().add(bank.getDlugoscKolejki(), Color.BLUE);

		controller.setD3( new Diagram(DiagramType.TIME_FUNCTION, "Liczba klientow w oddziale"));
		controller.getD3().add(bank.getLiczbaKlientow(), Color.RED);

		controller.setD4(new Diagram(DiagramType.HISTOGRAM, "Czas oczekiwania na rozpoczecie obslugi"));
		controller.getD4().add(bank.getCzasOczekiwania(), Color.BLACK);

		controller.setD5(new Diagram(DiagramType.HISTOGRAM, "Prawdopodobienstwo rezygnacji z obslugi przez klienta"));
		controller.getD5().add(bank.getSzansaRezygnacji(), Color.BLACK);


		controller.print("\nSredni czas obslugi: " + String.format("%.2f",Statistics.arithmeticMean(bank.getCzasObslugi())));
		controller.print("Srednia liczba klientow w kolejce: " +  String.format("%.2f",Statistics.arithmeticMean(bank.getDlugoscKolejki())));
		controller.print("Srednia liczba klientow w oddziale: " +  String.format("%.2f",Statistics.arithmeticMean(bank.getLiczbaKlientow())));
		controller.print("Sredni czas oczekiwania na obsluge: " +  String.format("%.2f",Statistics.arithmeticMean(bank.getCzasOczekiwania())));
		controller.print("Srednie prawdopodobienstwo rezygnacji z obslugi przez klienta: " +  String.format("%.2f",Statistics.arithmeticMean(bank.getSzansaRezygnacji())));


	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub


	}

}
