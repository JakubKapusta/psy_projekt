package symulacja;

import java.text.DecimalFormat;
import java.util.Random;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;

public class ZakonczenieObslugi extends BasicSimEvent<Bank, Klient> {

	public ZakonczenieObslugi(Bank entity, double delay, Klient params) throws SimControlException {
		super(entity, delay, params);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return this.transitionParams;
	}

	@Override
	protected void onInterruption() throws SimControlException {
		// TODO Auto-generated method stub
        Klient klient = (Klient) getEventParams();
        App.controller.print("\nKlient [" + klient.getId() + "] zosta� przeniesiony do kolejki technicznej\n");
        klient.setPrzeniesiony(true);

	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void stateChange() throws SimControlException {
		// TODO Auto-generated method stub

		Bank bank = getSimObj();

		Klient klient = (Klient) getEventParams();

        if(!klient.isZniecierpliwiony()) {


                klient.setObslugiwany(false);
                if(klient.isPrzeniesiony())
                    klient.setPrzeniesiony(false);

                bank.getOkienka()[klient.getNrOkienka()].setWolne(true);
                bank.getOkienka()[klient.getNrOkienka()].obecnyKlient = null;



                DecimalFormat f = new DecimalFormat("##.00");


                String text = Boolean.toString(bank.getOkienka()[0].isWolne());
                StringBuilder sB = new StringBuilder(text);

                for(int i=1; i<bank.getLiczbaOkienek(); i++)
                    sB.append(" " + bank.getOkienka()[i].isWolne());

                text = sB.toString();

                if (simTime() < 1)
                    App.controller.print("[0" + String.format("%" + 3 + "." + 3 + "s", f.format(simTime()))
                            + "] :: Klasa zdarzenia: [" + String.format("%" + 19 + "." + 19 + "s", this.getClass().getSimpleName())
                            + "] :: nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", klient.getId())
                            + "] :: pr_klienta: [" + String.format("%" + 1 + "." + 1 + "s", klient.getPriorytet())
                            //+ "] :: id_okienka: [" + String.format("%" + 2 + "." + 2 + "s", k.nrOkienka)
                            + "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
                            + "] :: wolne okienka[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));

                else
                    App.controller.print("[" + String.format("%" + 4 + "." + 4 + "s", f.format(simTime()))
                            + "] :: Klasa zdarzenia: [" + String.format("%" + 19 + "." + 19 + "s", this.getClass().getSimpleName())
                            + "] :: nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", klient.getId())
                            + "] :: pr_klienta: [" + String.format("%" + 1 + "." + 1 + "s", klient.getPriorytet())
                            //+ "] :: id_okienka: [" + String.format("%" + 2 + "." + 2 + "s", k.nrOkienka)
                            + "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
                            + "] :: wolne okienka[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));




                //decyzja o ponownym wstawieniu klienta do kolejki
                Random r = new Random();
                if(r.nextInt(100) < bank.getSzansaPowrotu()){
                    bank.getKolejka().add(klient);
                    App.controller.print("\nKlient [" + klient.getId() + "] ponownie wr�ci� do kolejki\n");
                }

                else {
                    //statystyki

                    klient.setCzasOdejscia(simTime());

                    bank.getCzasObslugi().setValue(klient.getCzasOdejscia() - klient.getCzasPrzybycia());

                    bank.getDlugoscKolejki().setValue(bank.getKolejka().size(), simTime());

                    //dekrementacja liczby klient�w w oddziale
                    bank.decLicznoscOddzialu();
                    klient.getZniecierpliwienie().interrupt();

                }

       }
        else {
            //zako�czenie obs�ugi nie ma sensu w sytuacji, gdy klient si� ju� zniecierliwi�
            return;

        }

             new RozpoczecieObslugi(bank, 0);

		}





}
