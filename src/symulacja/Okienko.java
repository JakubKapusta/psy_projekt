package symulacja;

public class Okienko {
    private int id;
    private boolean wolne;
    private boolean awaria;

    Klient obecnyKlient;

    public Okienko(int id){
        this.id = id;
        this.wolne = true;
        this.awaria = false;
    }



    public boolean isWolne() {
        return wolne;
    }

    public boolean isAwaria() {
        return awaria;
    }

    public void setWolne(boolean wolne) {
        this.wolne = wolne;
    }

    public void setAwaria(boolean awaria) {
        this.awaria = awaria;
    }

}
