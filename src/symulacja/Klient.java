package symulacja;

public class Klient {

	private int id;
	private Zniecierpliwienie zniecierpliwienie;
	private ZakonczenieObslugi zakonczenieObslugi;

	private int nrOkienka,priorytet;
	private double czasPrzybycia;
	private double czasObsluzenia;
	private double czasOdejscia;


	private boolean zniecierpliwiony;
	private boolean obslugiwany;

	private boolean przeniesiony;


	public Klient(int id) {
		this.id = id;
		this.zniecierpliwiony=false;
		this.obslugiwany=false;
		this.przeniesiony = false;
	}

	public int getId() {
		return id;
	}

	public Zniecierpliwienie getZniecierpliwienie() {
		return zniecierpliwienie;
	}

	public ZakonczenieObslugi getZakonczenieObslugi() {
		return zakonczenieObslugi;
	}

	public int getNrOkienka() {
		return nrOkienka;
	}

	public int getPriorytet() {
		return priorytet;
	}

	public void setNrOkienka(int nrOkienka) {
		this.nrOkienka = nrOkienka;
	}
	public double getCzasPrzybycia() {
		return czasPrzybycia;
	}
	public double getCzasObsluzenia() {
		return czasObsluzenia;
	}

	public void setCzasObsluzenia(double czasObsluzenia) {
		this.czasObsluzenia = czasObsluzenia;
	}

	public double getCzasOdejscia() {
		return czasOdejscia;
	}

	public boolean isZniecierpliwiony() {
		return zniecierpliwiony;
	}

	public boolean isObslugiwany() {
		return obslugiwany;
	}

	public void setZniecierpliwienie(Zniecierpliwienie zniecierpliwienie) {
		this.zniecierpliwienie = zniecierpliwienie;
	}

	public void setZakonczenieObslugi(ZakonczenieObslugi zakonczenieObslugi) {
		this.zakonczenieObslugi = zakonczenieObslugi;
	}

	public void setCzasPrzybycia(double czasPrzybycia) {
		this.czasPrzybycia = czasPrzybycia;
	}

	public void setCzasOdejscia(double czasOdejscia) {
		this.czasOdejscia = czasOdejscia;
	}

	public void setZniecierpliwiony(boolean zniecierpliwiony) {
		this.zniecierpliwiony = zniecierpliwiony;
	}

	public void setObslugiwany(boolean obslugiwany) {
		this.obslugiwany = obslugiwany;
	}
	public void setPriorytet(int priorytet) {
		this.priorytet = priorytet;
	}

	public boolean isPrzeniesiony() {
		return przeniesiony;
	}

	public void setPrzeniesiony(boolean przeniesiony) {
		this.przeniesiony = przeniesiony;
	}

}
