package symulacja;

import java.util.LinkedList;

import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.monitors.MonitoredVar;
import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;

public class Bank extends BasicSimObj {
	
	//kolejka
	
	private LinkedList<Klient> kolejka= new LinkedList<Klient>();
	private LinkedList<Klient> kolejkaTechniczna = new LinkedList<>();
	private Okienko [] okienka;


	private double mi;

	private SimGenerator random;
	private double zdatnosc;


	private int liczbaPriorytetow;

	private int liczbaOkienek;

	private int szansaPowrotu;


	private int liczbaRezygnujacych;
	private int sumaKlientow;

	private int limitKlientow;

	private int licznoscOddzialu;

	private MonitoredVar czasObslugi;
	private MonitoredVar dlugoscKolejki;
	private MonitoredVar liczbaKlientow;

	private MonitoredVar szansaRezygnacji;


	private MonitoredVar czasOczekiwania;

	private double niezdatnosc;

	private double parZniecierpliwienia;

	

	public Bank(double mi, int liczbaPriorytetow, int liczbaOkienek, int szansaPowrotu, int limitKlientow, SimGenerator random, double zdatnosc, double niezdatnosc, double parZniecierpliwienia) {
		super();
		this.mi = mi;
		this.liczbaPriorytetow = liczbaPriorytetow;
		this.liczbaOkienek = liczbaOkienek;
		this.okienka = new Okienko[liczbaOkienek];
		this.szansaPowrotu = szansaPowrotu;
		this.limitKlientow = limitKlientow;
		this.licznoscOddzialu=0;
		this.random = random;
		this.zdatnosc=zdatnosc;
		this.niezdatnosc=niezdatnosc;
		this.czasObslugi = new MonitoredVar();
		this.dlugoscKolejki = new MonitoredVar();
		this.liczbaKlientow = new MonitoredVar();
		this.czasOczekiwania = new MonitoredVar();
		this.szansaRezygnacji = new MonitoredVar();
		this.liczbaRezygnujacych=0;
		this.sumaKlientow=0;
		this.parZniecierpliwienia = parZniecierpliwienia;

		stworzOkienka();
	}

	public void wstawKlienta(Klient klient){

		kolejka.add(klient);
		if(kolejka.size() == 1 && jestWolneOkienko()){
			try {
				new RozpoczecieObslugi(this, 0);
			} catch (SimControlException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void stworzOkienka(){
		for(int i=0; i<liczbaOkienek; i++)
			okienka[i] = new Okienko(i + 1);
	}


	public boolean jestWolneOkienko(){
		boolean flag =  true;

		for(int i=0; i<okienka.length; i++)
			if(!okienka[i].isWolne()) //|| okienka[i].isAwaria())
				flag = false;

		return flag;

	}

	public int nrWolnegoOkienka(){

		for(int i=0; i<okienka.length; i++)
			if(okienka[i].isWolne())
				return i;

		return -1;
	}


	public LinkedList<Klient> getKolejka() {
		return kolejka;
	}

	public LinkedList<Klient> getKolejkaTechniczna() {
		return kolejkaTechniczna;
	}

	public Okienko[] getOkienka() {
		return okienka;
	}

	public double getMi() {
		return mi;
	}

	public MonitoredVar getLiczbaKlientow() {
		return liczbaKlientow;
	}

	public SimGenerator getRandom() {
		return random;
	}

	public double getZdatnosc() {
		return zdatnosc;
	}

	public int getLiczbaPriorytetow() {
		return liczbaPriorytetow;
	}

	public int getLiczbaOkienek() {
		return liczbaOkienek;
	}

	public int getSzansaPowrotu() {
		return szansaPowrotu;
	}

	public MonitoredVar getCzasObslugi() {
		return czasObslugi;
	}

	public MonitoredVar getDlugoscKolejki() {
		return dlugoscKolejki;
	}

	public double getNiezdatnosc() {
		return niezdatnosc;
	}

	public MonitoredVar getCzasOczekiwania() {
		return czasOczekiwania;
	}

	public int getLiczbaRezygnujacych() {
		return liczbaRezygnujacych;
	}

	public void incLiczbaRezygnujacych() {
		this.liczbaRezygnujacych++;
	}

	public int getSumaKlientow() {
		return sumaKlientow;
	}

	public void incSumaKlientow() {
		this.sumaKlientow++;
	}

	public void incLicznoscOddzialu() {
		this.licznoscOddzialu++;
	}

	public void decLicznoscOddzialu() {
		this.licznoscOddzialu--;
	}

	public int getLimitKlientow() {
		return limitKlientow;
	}

	public int getLicznoscOddzialu() {
		return licznoscOddzialu;
	}

	public MonitoredVar getSzansaRezygnacji() {
		return szansaRezygnacji;
	}

	public double getParZniecierpliwienia() {
		return parZniecierpliwienia;
	}



	@Override
	public boolean filter(IPublisher arg0, INotificationEvent arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void reflect(IPublisher arg0, INotificationEvent arg1) {
		// TODO Auto-generated method stub

	}

}
