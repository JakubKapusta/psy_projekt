package symulacja;

import java.text.DecimalFormat;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;

public class Zniecierpliwienie extends BasicSimEvent<Bank, Klient> {
	
	
	public Zniecierpliwienie(Bank entity, double delay, Klient params) throws SimControlException {
		super(entity, delay, params);
		// TODO Auto-generated constructor stub
	}



	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return transitionParams;
	}

	@Override
	protected void onInterruption() throws SimControlException {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void stateChange() throws SimControlException {
		// TODO Auto-generated method stub
		
		Bank bank = getSimObj();
		Klient klient = (Klient)getEventParams();

		klient.setZniecierpliwiony(true);

		bank.decLicznoscOddzialu();

		//usuni�cie klienta z kolejki lub jego odej�cie w momencie stania przy okienku

		if(!bank.getKolejka().isEmpty() && !klient.isObslugiwany())
			bank.getKolejka().remove(klient);

		//zwolnienie okienka
		if(klient.isObslugiwany()) {
			bank.getOkienka()[klient.getNrOkienka()].setWolne(true);
			bank.getOkienka()[klient.getNrOkienka()].obecnyKlient = null;
			new RozpoczecieObslugi(bank,0);
		}

		if(klient.isPrzeniesiony())
			bank.getKolejkaTechniczna().remove(klient);

		//statystyki
		bank.incLiczbaRezygnujacych();
		bank.getSzansaRezygnacji().setValue((double)bank.getLiczbaRezygnujacych()/bank.getSumaKlientow(), simTime());


		String text = Boolean.toString(bank.getOkienka()[0].isWolne());
		StringBuilder sB = new StringBuilder(text);

		for(int i=1; i<bank.getLiczbaOkienek(); i++)
			sB.append(" " + bank.getOkienka()[i].isWolne());

		text = sB.toString();


		DecimalFormat f = new DecimalFormat("##.00");
		if(simTime() < 1)
			App.controller.print("[0"
					+ String.format("%" + 3 + "." + 3 + "s", f.format(simTime())) 
					+ "] :: Klasa zdarzenia: [" + String.format("%" + 24 + "." + 24 + "s", this.getClass().getSimpleName())
					+ "] :: nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", klient.getId())
					+ "] :: pr_klienta: [" + String.format("%" + 1 + "." + 1 + "s", klient.getPriorytet())
					//+ "] :: id_okienka: [" + String.format("%" + 2 + "." + 2 + "s", klient.nrOkienka)
					+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
					+ "] :: wolne okienka[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));
			
			else
			App.controller.print("["
						+ String.format("%" + 4 + "." + 4 + "s", f.format(simTime())) 
						+ "] :: Klasa zdarzenia: [" + String.format("%" + 24 + "." + 24 + "s", this.getClass().getSimpleName())
						+ "] :: nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", klient.getId())
						+ "] :: pr_klienta: [" + String.format("%" + 1 + "." + 1 + "s", klient.getPriorytet())
						//+ "] :: id_okienka: [" + String.format("%" + 2 + "." + 2 + "s", klient.nrOkienka)
						+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
						+ "] :: wolne okienka[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));
			
		}
		

		
		


}
