package symulacja;

import java.text.DecimalFormat;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;

public class RozpoczecieObslugi extends BasicSimEvent<Bank, Object> {

	public RozpoczecieObslugi(Bank entity, double delay) throws SimControlException {
		super(entity, delay);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void onInterruption() throws SimControlException {
		// TODO Auto-generated method stub

		
	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub

	}

	public int nrWolnegoOkienka(Bank bank){

		for(int i=0; i<bank.getOkienka().length; i++)
			if(bank.getOkienka()[i].isWolne())
				return i;

		return -1;
	}



	public int indeksKlienta(Bank bank){

		//pobranie klient�w wed�ug priorytetu (najpierw ten z najni�szym priorytetem)
		int minPriorytet = bank.getLiczbaPriorytetow();

		//znalezienie najmniejszego priorytetu w kolejce
		for(Klient klient : bank.getKolejka())
			if(klient.getPriorytet()<minPriorytet)
				minPriorytet = klient.getPriorytet();

		//znalezienie pierwszego klienta z takim priorytetem

		for(int i =0; i<bank.getKolejka().size(); i++)
			if(bank.getKolejka().get(i).getPriorytet() == minPriorytet)
				return i;

			return -1;

	}

	@Override
	protected void stateChange() throws SimControlException {
		// TODO Auto-generated method stub

		Bank bank = getSimObj();
		
		if(bank.getKolejka().size()==0)
			return;
		

		int idOkienka = nrWolnegoOkienka(bank);


		if(idOkienka >= 0) {

			Klient klient;

			//w pierwszej kolejno�ci nale�y pobiera� klient�w z kolejki technicznej
			if(bank.getKolejkaTechniczna().size() > 0){

				klient = bank.getKolejkaTechniczna().removeFirst();
				klient.setPrzeniesiony(false);
			}


			else {


				int index = indeksKlienta(bank);

				klient = bank.getKolejka().remove(index);
			}

			bank.getOkienka()[idOkienka].obecnyKlient = klient;

			klient.setObslugiwany(true);

			//przypisanie nrOkienka
			klient.setNrOkienka(idOkienka);

			bank.getOkienka()[idOkienka].setWolne(false);

			double dt = bank.getRandom().exponential(1.0 / bank.getMi());

			//brak awarii
			klient.setZakonczenieObslugi(new ZakonczenieObslugi(bank, dt, klient));



			//obliczenie statystyki

			klient.setCzasObsluzenia(simTime());
			bank.getCzasOczekiwania().setValue(klient.getCzasObsluzenia() - klient.getCzasPrzybycia());



			DecimalFormat f = new DecimalFormat("##.00");


			String text = Boolean.toString(bank.getOkienka()[0].isWolne());
			StringBuilder sB = new StringBuilder(text);

			for(int i=1; i<bank.getLiczbaOkienek(); i++)
				sB.append(" " + bank.getOkienka()[i].isWolne());

			text = sB.toString();

			if (simTime() < 1)
				App.controller.print("[0"
						+ String.format("%" + 3 + "." + 3 + "s", f.format(simTime()))
						+ "] :: Klasa zdarzenia: [" + String.format("%" + 19 + "." + 19 + "s", this.getClass().getSimpleName())
						+ "] :: nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", klient.getId())
						+ "] :: pr_klienta: [" + String.format("%" + 1 + "." + 1 + "s", klient.getPriorytet())
						//+ "] :: id_okienka: [" + String.format("%" + 2 + "." + 2 + "s", idOkienka)
						+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
						+ "] :: wolne okienka[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));


			else
				App.controller.print("["
						+ String.format("%" + 4 + "." + 4 + "s", f.format(simTime()))
						+ "] :: Klasa zdarzenia: [" + String.format("%" + 19 + "." + 19 + "s", this.getClass().getSimpleName())
						+ "] :: nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", klient.getId())
						+ "] :: pr_klienta: [" + String.format("%" + 1 + "." + 1 + "s", klient.getPriorytet())
						//+ "] :: id_okienka: [" + String.format("%" + 2 + "." + 2 + "s", idOkienka)
						+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
						+ "] :: wolne okienka[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));

		}

		else{
			App.controller.print("\nW chwili obecnej nie ma �adnego wolnego okienka\n");
		}
		
	}

}
