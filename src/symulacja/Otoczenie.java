package symulacja;

import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimObj;

public class Otoczenie extends BasicSimObj{

	public Otoczenie(Bank bank, SimGenerator random, double lambda) {
		super();
		this.bank = bank;
		this.random = random;
		this.lambda = lambda;
		this.clientCounter = 1;
	}

	private int clientCounter;
	
	private Bank bank;
	
	private SimGenerator random;
	
	private double lambda;


	public int incClientCounter() {
		return clientCounter++;

	}


	public Bank getBank() {
		return bank;
	}


	public SimGenerator getRandom() {
		return random;
	}


	public double getLambda() {
		return lambda;
	}

	@Override
	public boolean filter(IPublisher arg0, INotificationEvent arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void reflect(IPublisher arg0, INotificationEvent arg1) {
		// TODO Auto-generated method stub

	}

}
