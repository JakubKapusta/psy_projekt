package symulacja;

import java.text.DecimalFormat;
import java.util.Random;

import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;

public class WystapienieAwarii extends BasicSimEvent<Bank, Object> {
	
	 private SimGenerator generator = new SimGenerator();

	public WystapienieAwarii(Bank entity, double delay) throws SimControlException {
		super(entity, delay);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void onInterruption() throws SimControlException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void stateChange() throws SimControlException {
		// TODO Auto-generated method stub
		
		
		
		Bank bank = getSimObj();

		//wylosowanie okienka, kt�rego ma dotyczy� awaria
		Random r = new Random();
		int indexOkienka = r.nextInt(bank.getLiczbaOkienek());


		if(bank.getOkienka()[indexOkienka].isAwaria()){
			//po wylosowaniu okienka, kt�re jest ju� uszkodzone, wychodzimy z metody
			return;
		}

		else {

			//przes�anie klienta do kolejki technicznej

			if(bank.getOkienka()[indexOkienka].obecnyKlient != null) {
				bank.getKolejkaTechniczna().add(bank.getOkienka()[indexOkienka].obecnyKlient);
				bank.getOkienka()[indexOkienka].obecnyKlient.getZakonczenieObslugi().interrupt();
				bank.getOkienka()[indexOkienka].obecnyKlient.setObslugiwany(false);
				bank.getOkienka()[indexOkienka].obecnyKlient.setPrzeniesiony(true);
			}


			bank.getOkienka()[indexOkienka].setAwaria(true);
			String text = Boolean.toString(bank.getOkienka()[0].isAwaria());
			StringBuilder sB = new StringBuilder(text);

			for(int i=1; i<bank.getLiczbaOkienek(); i++)
				sB.append(" " + bank.getOkienka()[i].isAwaria());

			text = sB.toString();


			DecimalFormat f = new DecimalFormat("##.00");
			if (simTime() < 1)
				App.controller.print("[0"
						+ String.format("%" + 3 + "." + 3 + "s", f.format(simTime()))
						+ "] :: Klasa zdarzenia: [" + String.format("%" + 19 + "." + 19 + "s", this.getClass().getSimpleName())
						+ "] :: " + "nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", "--")
						+ "] :: " + "nrOkienka: [" + String.format("%" + 2 + "." + 2 + "s", indexOkienka+1)
						+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
						+ "] :: awaria okienek[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));

			else
				App.controller.print("["
						+ String.format("%" + 4 + "." + 4 + "s", f.format(simTime()))
						+ "] :: Klasa zdarzenia: [" + String.format("%" + 19 + "." + 19 + "s", this.getClass().getSimpleName())
						+ "] :: " + "nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", "--")
						+ "] :: " + "nrOkienka: [" + String.format("%" + 2 + "." + 2 + "s", indexOkienka+1)
						+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
						+ "] :: awaria okienek[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));


			//zaplanowanie kolejnej awarii oraz naprawy
			double dt = generator.exponential(bank.getNiezdatnosc());
			new WystapienieAwarii(bank,dt);

			double delay = generator.exponential(bank.getZdatnosc());
			new WystapienieNaprawy(bank, delay, indexOkienka);





		}
	}

	
}
