package symulacja;

import java.text.DecimalFormat;

import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;

public class WystapienieNaprawy extends BasicSimEvent<Bank, Integer> {
	
	
	private SimGenerator generator = new SimGenerator();

	public WystapienieNaprawy(Bank entity, double delay, Integer params) throws SimControlException {
		super(entity, delay, params);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return transitionParams;
	}

	@Override
	protected void onInterruption() throws SimControlException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void stateChange() throws SimControlException {
		// TODO Auto-generated method stub
		
		Bank bank = getSimObj();

		//pobranie indexu uszkodzonego okienka
		int indexOkienka = (int) getEventParams();
		bank.getOkienka()[indexOkienka].setAwaria(false);
		bank.getOkienka()[indexOkienka].setWolne(true);

		// po naprawie okienka, nale�y rozpocz�� obs�ug�
		new RozpoczecieObslugi(bank, 0);

		String text = Boolean.toString(bank.getOkienka()[0].isAwaria());

		StringBuilder sB = new StringBuilder(text);
		for(int i=1; i<bank.getLiczbaOkienek(); i++)
			sB.append(" " + Boolean.toString(bank.getOkienka()[i].isAwaria()));


		text = sB.toString();


		DecimalFormat f = new DecimalFormat("##.00");
		
		if(simTime() < 1)
            App.controller.print("[0"
			+ String.format("%" + 3 + "." + 3 + "s", f.format(simTime())) 
			+ "] :: Klasa zdarzenia: [" + String.format("%" + 19 + "." + 19 + "s", this.getClass().getSimpleName())
			+ "] :: "  +"nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", "--")
			+ "] :: " + "nrOkienka: [" + String.format("%" + 2 + "." + 2 + "s", indexOkienka+1)
			+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
					+ "] :: awaria okienek[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));
		
			else
            App.controller.print("["
				+ String.format("%" + 4 + "." + 4 + "s", f.format(simTime())) 
				+ "] :: Klasa zdarzenia: [" + String.format("%" + 19 + "." + 19 + "s", this.getClass().getSimpleName())
				+ "] :: "  +"nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", "--")
				+ "] :: " + "nrOkienka: [" + String.format("%" + 2 + "." + 2 + "s", indexOkienka+1)
				+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", bank.getKolejka().size())
				+ "] :: awaria okienek[" + String.format("%" + 5*(bank.getLiczbaOkienek()+2) + "." + 5*(bank.getLiczbaOkienek()+2) + "s", text + "]"));


		
		double delay = generator.exponential(bank.getNiezdatnosc());
		new WystapienieAwarii(bank, delay);

	}
	
	

}
