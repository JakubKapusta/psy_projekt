package symulacja;

import java.text.DecimalFormat;
import java.util.Random;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;

public class PrzybycieKlienta extends BasicSimEvent<Otoczenie, Object> {
	
	public PrzybycieKlienta(Otoczenie entity, double delay) throws SimControlException {
		super(entity, delay);
		// TODO Auto-generated constructor stub

	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void onInterruption() throws SimControlException {
		// TODO Auto-generated method stub


	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void stateChange() throws SimControlException {
			// TODO Auto-generated method stub


			Otoczenie otoczenie = getSimObj();

		if(otoczenie.getBank().getLicznoscOddzialu() < otoczenie.getBank().getLimitKlientow())
		{

			Klient klient = new Klient(otoczenie.incClientCounter());

			otoczenie.getBank().incLicznoscOddzialu();


			//wylosowanie priorytetu dla klienta
			Random r = new Random();
			klient.setPriorytet(r.nextInt(otoczenie.getBank().getLiczbaPriorytetow()) + 1);


			String text = Boolean.toString(otoczenie.getBank().getOkienka()[0].isWolne());
			StringBuilder sB = new StringBuilder(text);

			for(int i=1; i<otoczenie.getBank().getLiczbaOkienek(); i++)
				sB.append(" " + otoczenie.getBank().getOkienka()[i].isWolne());

			text = sB.toString();

			klient.setCzasPrzybycia(simTime());

			otoczenie.getBank().wstawKlienta(klient);
			
			
			//wygenerowanie zniecierpliwienia
			double odstep = otoczenie.getRandom().exponential(1.0/otoczenie.getBank().getParZniecierpliwienia());
			
			klient.setZniecierpliwienie(new Zniecierpliwienie(otoczenie.getBank(), odstep, klient));

			//generowanie RozpoczeciaObslugi do momentu zape�nienia okienek

			if(klient.getId() <= otoczenie.getBank().getLiczbaOkienek() || otoczenie.getBank().nrWolnegoOkienka() > 0)
				new RozpoczecieObslugi(otoczenie.getBank(), 0);


			DecimalFormat f = new DecimalFormat("##.00");

			
			if(simTime() < 1)
				App.controller.print("[0"
				+ String.format("%" + 3 + "." + 3 + "s", f.format(simTime())) 
				+ "] :: Klasa zdarzenia: [" + String.format("%" + 23 + "." + 23 + "s", this.getClass().getSimpleName())
				+ "] :: nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", klient.getId())
				+ "] :: pr_klienta: [" + String.format("%" + 1 + "." + 1 + "s", klient.getPriorytet())
				+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", otoczenie.getBank().getKolejka().size())
						+ "] :: wolne okienka[" + String.format("%" + 5*(otoczenie.getBank().getLiczbaOkienek()+2) + "." + 5*(otoczenie.getBank().getLiczbaOkienek()+2) + "s", text + "]"));
			
				else
                App.controller.print("["
							+ String.format("%" + 4 + "." + 4 + "s", f.format(simTime())) 
							+ "] :: Klasa zdarzenia: [" + String.format("%" + 23 + "." + 23 + "s", this.getClass().getSimpleName())
							+ "] :: nr_klienta: [" + String.format("%" + 3 + "." + 3 + "s", klient.getId())
							+ "] :: pr_klienta: [" + String.format("%" + 1 + "." + 1 + "s", klient.getPriorytet())
							+ "] :: d�ugo�� kolejki: [" + String.format("%" + 2 + "." + 2 + "s", otoczenie.getBank().getKolejka().size())
							+ "] :: wolne okienka[" + String.format("%" + 5*(otoczenie.getBank().getLiczbaOkienek()+2) + "." + 5*(otoczenie.getBank().getLiczbaOkienek()+2) + "s", text + "]"));

				//natychmiastowe rozpocz�cie obs�ugi


				//dodanie statystyki
				otoczenie.getBank().getLiczbaKlientow().setValue(klient.getId(), simTime());
				otoczenie.getBank().incSumaKlientow();

		}
		else
			App.controller.print("\nNowy klient nie wejdzie do banku - zosta� przekroczony limit klient�w w oddziale wynosz�cy " + otoczenie.getBank().getLimitKlientow() + "\n");

		double dt = otoczenie.getRandom().exponential(1.0/otoczenie.getLambda());
		new PrzybycieKlienta(otoczenie, dt);
	}

}
